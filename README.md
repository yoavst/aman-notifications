Aman Notifications
===========

![Sample output](http://i.imgur.com/uniBfx0.png)

## Usage
Run the following command to trigger the notification:

```java -jar aman_v1.jar id password telegramBotApiKey telegramUserId```

* **id** - the passport id
* **password** - the password for the website
* **telegramBotApiKey** - the api key for your created telegram bot
* **telegramUserId** - your telegram's account user id

## Compiling
```gradlew shadowjar```

File will be generated on `build/libs` folder.

## Creating telegram bot
Follow steps 1 & 2 in [this guide](https://www.forsomedefinition.com/automation/creating-telegram-bot-notifications/).

## License

    Copyright 2016 Yoav Sternberg

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.