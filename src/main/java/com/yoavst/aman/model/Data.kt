package com.yoavst.aman.model

data class Data(val routes: List<Route> = emptyList(), val questionnaires: List<Questionnaire> = emptyList(), val events: List<Event> = emptyList())
