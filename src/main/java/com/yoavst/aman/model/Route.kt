package com.yoavst.aman.model

data class Route(val name: String, val status: String)