package com.yoavst.aman.model

data class Event(val name: String, val location: String, val date: Long, val hasApproved: Boolean)