package com.yoavst.aman.model

data class Questionnaire(val name: String, val instructions: String, val lastDate: Long, val status: String)