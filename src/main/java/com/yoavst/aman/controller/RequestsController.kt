package com.yoavst.aman.controller

import com.yoavst.aman.model.Data

interface RequestsController {
    fun login(success: () -> Unit, failure: (errorType: Int) -> Unit)
    fun query(success: (data: Data) -> Unit, failure: () -> Unit)

    companion object {
        const val NetworkError = 0
        const val IdError = -1
        const val PasswordError = -2
    }
}