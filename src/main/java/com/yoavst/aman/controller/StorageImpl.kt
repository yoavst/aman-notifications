package com.yoavst.aman.controller

import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.yoavst.aman.model.Data
import java.io.File

object StorageImpl : Storage {
    override var data: Data?
        get() {
            val text = CacheFile.readText()
            if (text.isEmpty()) return null
            else return gson.fromJson(CacheFile.reader(Charsets.UTF_8))
        }
        set(value) {
            if (value == null)
                CacheFile.writeText("")
            else
                CacheFile.writeText(gson.toJson(value))

        }

    private val gson = Gson()
    private val CacheFile = File(System.getProperty("user.home"), "aman_notifications.json").apply { createNewFile() }
}