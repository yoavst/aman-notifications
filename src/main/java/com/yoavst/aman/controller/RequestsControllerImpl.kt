package com.yoavst.aman.controller

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.google.gson.JsonParser
import com.yoavst.aman.model.Data
import java.net.URI
import java.text.SimpleDateFormat
import com.yoavst.aman.controller.RequestsController.Companion.IdError
import com.yoavst.aman.controller.RequestsController.Companion.PasswordError
import com.yoavst.aman.controller.RequestsController.Companion.NetworkError
import com.yoavst.aman.model.Event
import com.yoavst.aman.model.Questionnaire
import com.yoavst.aman.model.Route
import org.jsoup.Jsoup
import java.net.CookieManager
import java.nio.charset.Charset

class RequestsControllerImpl(private val id: String, private val password: String) : RequestsController {
    private val cookieManager = CookieManager()

    override fun login(success: () -> Unit, failure: (Int) -> Unit) {
        AuthUrl.httpPost()
                .header("Content-type" to "application/json; charset=UTF-8")
                .body("""{misparMoamad: "$id", userPass: "$password"}""")
                .responseString { request, response, result ->
                    result.fold(success = {
                        val data = JsonParser().parse(it).asJsonObject["d"].asJsonObject
                        val serverId = data["MisparMoamad"].asInt
                        if (serverId == IdError || serverId == PasswordError) failure(serverId)
                        else {
                            cookieManager.put(URI(AuthUrl), response.httpResponseHeaders)
                            success()
                        }
                    }, failure = {
                        failure(NetworkError)
                    })
                }
    }

    override fun query(success: (Data) -> Unit, failure: () -> Unit) {
        DataUrl.httpGet().header("Cookie" to cookieManager.cookieStore.cookies.joinToString(";"))
                .responseObject(deserializer) { request, response, result ->
                    result.fold(success = {
                        cookieManager.put(URI(AuthUrl), response.httpResponseHeaders)
                        val document = Jsoup.parse(it)


                        val routes: List<Route> = document.getElementById("ctl00_ctl00_cphMain_CPHMainContent_divSection1")?.run {
                            children().drop(1).map {
                                Route(
                                        name = it.getElementsByClass("CourseName")[0].text(),
                                        status = it.getElementsByClass("CourseStatus")[0].text()
                                )
                            }
                        } ?: emptyList()

                        val questionnaires: List<Questionnaire> = document.getElementById("ctl00_ctl00_cphMain_CPHMainContent_divSecrion2")?.run {
                            children().drop(1).map {
                                Questionnaire(
                                        name = it.getElementsByClass("SurveyName")[0].text(),
                                        instructions = it.getElementsByClass("SurveyInstructions")[0].text(),
                                        lastDate = DateParser.parse(it.getElementsByClass("SurveyLastDate")[0].text()).time,
                                        status = it.getElementsByClass("SurveyStatus")[0].text()
                                )
                            }
                        } ?: emptyList()

                        val events: List<Event> = document.getElementById("ctl00_ctl00_cphMain_CPHMainContent_divSection3")?.run {
                            getElementsByClass("TableRow").map {
                                Event(
                                        name = it.getElementsByClass("EventName")[0].text(),
                                        location = it.getElementsByClass("EventPlace")[0].text(),
                                        date = FullDateParser.parse(it.getElementsByClass("EventDate")[0].text()).time,
                                        hasApproved = it.children().last().text().contains("אושר")
                                )
                            }
                        } ?: emptyList()

                        success(Data(routes, questionnaires, events))
                    }, failure = {
                        failure()
                    })
                }
    }

    companion object {
        private val DateParser = SimpleDateFormat("dd/MM/yyyy")
        private val FullDateParser = SimpleDateFormat("dd/MM/yyyy ב-HH:mm")
        private val AuthUrl = "https://www.aman.idf.il/Modiin/AuthenticationService.asmx/Authenticate"
        private val DataUrl = "https://www.aman.idf.il/modiin/Kiosk.aspx?catId=59951"

        private val deserializer = object : ResponseDeserializable<String> {
            override fun deserialize(bytes: ByteArray): String {
                return String(bytes, Charset.forName("windows-1255"))
            }
        }
    }
}