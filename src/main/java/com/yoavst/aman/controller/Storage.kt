package com.yoavst.aman.controller

import com.yoavst.aman.model.Data

interface Storage {
    var data: Data?
}