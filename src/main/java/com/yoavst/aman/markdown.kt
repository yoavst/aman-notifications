package com.yoavst.aman

fun String.bold(): String {
    return "*$this*"
}
