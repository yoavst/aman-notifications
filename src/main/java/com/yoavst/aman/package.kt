package com.yoavst.aman

import com.github.kittinunf.fuel.httpGet
import com.yoavst.aman.controller.RequestsController
import com.yoavst.aman.controller.RequestsControllerImpl
import com.yoavst.aman.controller.Storage
import com.yoavst.aman.controller.StorageImpl
import com.yoavst.aman.model.Data
import java.text.SimpleDateFormat
import java.util.*
import kotlin.system.exitProcess

const val Name = "aman_v1.jar"

val storage: Storage = StorageImpl
val handlers: Array<(old: Data?, current: Data) -> Unit> = arrayOf(::notify)

var hasTriedAgainLogin = false
var hasTriedAgainQuery = false

fun main(args: Array<String>) {
    if (args.size != 4) {
        println("java -jar $Name id password telgeramApiKey telegramUserApiKey")
    } else {
        println("Running Notifications for Aman script.")
        val (id, password, telegramApiKey, telegramUserApiKey) = args
        TelegramApiKey = telegramApiKey
        TelegramUserApiKey = telegramUserApiKey

        val requestsController = RequestsControllerImpl(id, password)

        requestsController.login(success = {
            println("Successfully login to the website.")
            query(requestsController)
        }, failure = {
            if (!hasTriedAgainLogin) {
                println("Failed to login, trying again.")
                hasTriedAgainLogin = true
                Thread.sleep(60000)
                main(args)
            } else {
                println("Failed to login again.")
                exitProcess(1)
            }
        })
    }
}

private fun query(requestsController: RequestsController) {
    requestsController.query(success = { data ->
        println("Successfully queried website.")
        val old = storage.data
        handlers.forEach { it(old, data) }
        storage.data = data
        println("Finished updating")
        exitProcess(0)
    }, failure = {
        if (!hasTriedAgainQuery) {
            println("Failed to query, trying again.")
            hasTriedAgainQuery = true
            Thread.sleep(60000)
            query(requestsController)
        } else {
            println("Failed to query again.")
            exitProcess(1)
        }
    })
}

var TelegramApiKey = ""
var TelegramUserApiKey = ""
private val TelegramBaseAddress: String
    get() = "https://api.telegram.org/bot$TelegramApiKey/sendMessage"


private fun notify(old: Data?, current: Data) {
    if (old != current) {
        val text = toString(diff(old, current))
        val address = "$TelegramBaseAddress?chat_id=$TelegramUserApiKey&parse_mode=markdown&disable_web_page_preview=1&text=$text"

        val (request, response, result) = address.httpGet().responseString()
        println("Telegram request sent.")
    }
}

private fun diff(old: Data?, current: Data): Data {
    if (old == null) return current
    val routes = current.routes.filterNot { it in old.routes }
    val questionnaires = current.questionnaires.filterNot { it in old.questionnaires }
    val events = current.events.filterNot { it in old.events }
    return Data(routes, questionnaires, events)
}

private fun toString(data: Data): String {
    return buildString {
        appendln()
        if (data.routes.isNotEmpty()) {
            appendln("שינוי במסלולים:".bold())
            data.routes.forEach {
                appendln(it.name + ": " + it.status)
            }
        }
        if (data.questionnaires.isNotEmpty()) {
            appendln("שינוי בשאלונים:".bold())
            data.questionnaires.forEach {
                appendln(it.name + " - " + it.status)
            }
        }
        if (data.events.isNotEmpty()) {
            appendln("שינוי באירועים:".bold())
            data.events.forEach {
                appendln(it.name + " " + FullDateFormat.format(Date(it.date)))
            }
        }
    }
}

    private val FullDateFormat = SimpleDateFormat("dd/MM/yyyy ב-HH:mm")